# Repository für WHERESCAPE 3D Content #

## model_conversions
Die Model-Conversions - im Folgenden **MC** genannt - sind in der Datei model_conversions.xml gespeichert.

Es gibt aktive MC und MC in Entwicklung. 
Die aktiven MC sind diejenigen, die für die Überleitung von einem Layer zu Nächsten aktuell verwendet werden. 
Also zum Beispiel vom Data Vault Design zum Data Vault. 
Die aktiven MC müssen folgendes Naming haben:

**_** *brand* *Zielsystem* **- Step** *Schritt-Nummer*: *Titel*
- also z. B. 

**_areto DV - Step 3: Create Satellites**

Die MC sollten sortiert im 3D vorliegen. Folgende Schritte können angewendet werden um das zu realisieren.

- save model conversion zu model_conversion.xml
- git commit und push 
- starte [sort_model_conversions.bat](./sort_model_conversions.bat)
- lösche alle model conversions aus 3D
- import model_conversions.result.xml in 3D
- ggf. git commit und push
