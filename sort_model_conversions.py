#!/usr/bin/env python3
import argparse
import sys
import os

from xml.etree import cElementTree

SCRIPT_DIR = os.path.dirname(sys.argv[0])
WHERESCAPE_NS = "{http://www.wherescape.com/xml/3D}"


def get_parser(input_list):
    """
    read input parameters
    :param input_list:
    :return: dict of parameter values
    :return: parser object
    """
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--input',
                        dest='input',
                        default='model_conversions.xml',
                        help='input model conversion xml file')
    parser.add_argument('--result',
                        dest='result',
                        default='model_conversions.result.xml',
                        help='destination xml file')

    parser.add_argument('--element',
                        default='transformation',
                        help='element to search for')

    return parser.parse_args(input_list), parser


def tag_name(tag):
    return WHERESCAPE_NS + tag


def find_my_elm(node, element, node_dict=None):
    if node_dict is None:
        node_dict = {}
    for sub_node in list(node):
        my_tag = sub_node.tag
        if my_tag not in node_dict:
            node_dict[my_tag] = {element: []}
        if element.lower() in ([my_tag.lower()] + [x.lower() for x in my_tag.split('}')]):
            node_dict[my_tag][element].append(sub_node)
            continue
        node_dict[my_tag].update(find_my_elm(sub_node, element, {}))
    return node_dict


def sort_elements(tree, element):
    def sort_by_name(value):
        return value.attrib["name"].lower()

    for tag, node in tree.items():
        if node is not None and element in node:
            node[element].sort(key=sort_by_name)
        if tag != element:
            sort_elements(node, element)


def rearrange_tree(root, ordered_dictionary):
    current_node = root.find(tag_name("structure")).find(
        tag_name("transformations"))
    current_node.clear()
    for transformation in ordered_dictionary[tag_name("structure")][tag_name("transformations")][
            tag_name("transformation")]["transformation"]:
        current_node.append(transformation)
        
        
def main(input_args):
    tree = cElementTree.ElementTree(input_args.input)
    parsed_etree = tree.parse(input_args.input)
    dict_tree_elements = find_my_elm(parsed_etree, element=input_args.element)
    sort_elements(dict_tree_elements, input_args.element)
    cElementTree.register_namespace("", WHERESCAPE_NS)
    rearrange_tree(parsed_etree, dict_tree_elements)
    tree.write(input_args.result, "utf-8")


if __name__ == "__main__":
    args, _ = get_parser(sys.argv[1:])
    main(args)
